const IMG1 = "fas"
let id = 0

function createTodo(todoList, todoContent, className = "uncompleted", givenId = null, content = todoContent.value){
    const todoContainer = document.createElement('div');
    const todoItem = document.createElement('li');


    if(todoContent !== null){
        todoItem.textContent = todoContent.value;
        todoContent.value = ""
    }else{
        todoItem.textContent = content;
    }

    todoContainer.classList.add(className)

    const completedButton = document.createElement('button');
    completedButton.innerHTML =  '<i class="fas fa-check"></i>';
    completedButton.addEventListener("click", (event) => completeTodo(todoList))
    const deleteButton = document.createElement('button');
    deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
    deleteButton.addEventListener("click", (event) => deleteTodo(todoList))
    // todoItem.textContent = content
    todoContainer.classList.add(className);
    
    todoContainer.id = givenId === null ? Date.now() : givenId;

    todoContainer.appendChild(todoItem);
    todoContainer.appendChild(completedButton);
    todoContainer.appendChild(deleteButton);
    todoList.appendChild(todoContainer)
    if(!givenId){
        localStorage.setItem(`${Date.now()}`, JSON.stringify(todoContainer, ["id", "className", "tagName","innerText"]));
    }
    
}


export function addTodo(todoList, todoContent){
    event.preventDefault();
    if(todoContent.value === ""){return;}
    //create element
   createTodo(todoList,todoContent);
    
   
   
}

export function deleteTodo(todoList){
    let buttonElement = event.target;
    let containerElement = buttonElement.parentElement
    for(let divElement of todoList.children){
        if(containerElement === divElement){
            todoList.removeChild(divElement);
            localStorage.removeItem(divElement.id);
            break;
        }
    }
       
}

export function completeTodo(todoList){
    let buttonElement = event.target;
    let containerElement = buttonElement.parentElement;
    
    containerElement.classList.remove("uncompleted");
    containerElement.classList.add("completed");
    // let liElement = containerElement.children[0];

    //update class 
    // let storageItem = localStorage.getItem({})
    localStorage.setItem(containerElement.id, JSON.stringify(containerElement, ["id", "className", "tagName","innerText"]))
}

export function loadStorage(todoList){
    Object.keys(localStorage).forEach(function(key){
        let stringObject = localStorage.getItem(key)
        let liObject = JSON.parse(stringObject)
        createTodo(todoList, null, liObject.className, liObject.id, liObject.innerText)
     });
}