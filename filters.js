const $ = (s, p = document) => p.querySelector(s);

export function filterCompleted(todoList) {
    let items = todoList.children
    for (let todoItem of items) {
        if (todoItem.classList[0] === 'uncompleted') {
            todoItem.style.display = 'none'
        }
        else {
            todoItem.style.display = 'flex'
        }
    }
}

export function filterUncompleted(todoList) {
    let items = todoList.children
    for (let todoItem of items) {
        if (todoItem.classList[0] === 'completed') {
            todoItem.style.display = 'none'
        }
        else {
            todoItem.style.display = 'flex'
        }
    }
}

export function filterAll(todoList) {
    let items = todoList.children
    for (let todoItem of items) {
        todoItem.style.display = 'flex'
    }
}

