import * as filterApi from './filters.js'
import * as listManagerApi from './listManager.js'

const $ = (s, p = document) => p.querySelector(s);
const todosListArray = []
const todoContent = $(".newTodo");
const addTodoButton = $(".addTodoButton");
const todoList = $(".todos-list");
const filterCompletedButton = $(".filterCompleted")
const filterUnCompletedButton = $(".filterUncompleted")
const filterAllButton = $(".filterAll")


filterCompletedButton.addEventListener("click", () => filterApi.filterCompleted(todoList))
filterUnCompletedButton.addEventListener("click", () => filterApi.filterUncompleted(todoList))
filterAllButton.addEventListener("click", () => filterApi.filterAll(todoList))
addTodoButton.addEventListener("click", (event) => listManagerApi.addTodo(todoList, todoContent));

listManagerApi.loadStorage(todoList);


